'use strict';

/**
 * @ngdoc function
 * @name diyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the diyApp
 */
angular.module('diyApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
