'use strict';

/**
 * @ngdoc overview
 * @name diyApp
 * @description
 * # diyApp
 *
 * Main module of the application.
 */
angular
  .module('diyApp', [
    'ngAnimate',
    'ngTouch'
  ]);
